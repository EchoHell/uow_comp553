package life;

import java.io.InputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.TimeUnit;

public class ParallelLife {
	
	protected byte[][] new_board, grid;
	protected final int BOARD_SIZE;
	protected int generation;
	protected int numThreads = 1;
	private ForkJoinPool pool;
	private int rows_per_block = 1;
	private int num_blocks;
	
	public ParallelLife(int board_size, int threads){
		BOARD_SIZE = board_size;
		grid = new byte[board_size][board_size];
		this.numThreads = threads;
		num_blocks = grid.length / rows_per_block;
	}
	
	private void printBorder(){
		String line = "";
		for(int i = 0; i < BOARD_SIZE; i++)
			line+="-";
		System.out.println(" "+line+" ");
	}
	
	private void printBorder(int width){
		String line = "";
		for(int i = 0; i < width; i++)
			line+="-";
		System.out.println(" "+line+" ");
	}
	
	public void printBoard(){
		printBorder();
		for(int y = 0; y < BOARD_SIZE; y++){
			String line = "";
			for(int x = 0; x < BOARD_SIZE; x++){
				if(grid[y][x] == 1)
					line += "#";
				else
					line += " ";
			}
			System.out.println("|"+line+"|");
		}
		printBorder();
	}
	
	public void printBoard(int width, int height){
		if(width > BOARD_SIZE || height > BOARD_SIZE){
			System.out.println("Error: dimensions given are larger than board size.");
		} else {
			printBorder(width);
		
			for(int y = 0; y < height; y++){
				String line = "";
				for(int x = 0; x < width; x++){
					if(grid[y][x] == 1){
						line += "#";
					} else
						line += " ";;
				}
				System.out.println("|"+line+"|");
			}
			printBorder(width);
		}
	}
	
	public void parsePattern(InputStream s) throws Exception{	
		byte[][] _temp = new byte[BOARD_SIZE][BOARD_SIZE];
		char c;
		int xStart = Character.getNumericValue((char)s.read());
		int in = s.read();
		int x = xStart;
		int y = Character.getNumericValue((char)s.read());
		while(((char)s.read()) != '\n'){
			continue;
		}
		while((in = s.read()) != -1){
			c = (char)in;
			if(c == '#'){
				//System.out.println(x+":"+y+" is #");
				_temp[y][x] = 1;
				x++;
			} else if(c == '\n'){
				y++;
				x = xStart;
			} else {
				x++;
			}
		}
		
		this.grid = _temp;
		_temp = null;
	}
	
	public void shutDown(){
		//es.shutdown();
	}
	
	public void tick(){
		generation++;
		new_board = new byte[BOARD_SIZE][BOARD_SIZE];
		
		pool = new ForkJoinPool(numThreads);

		pool.execute(new Updater(0,grid.length,BOARD_SIZE,grid,new_board));
		
		pool.shutdown();
		try {
			pool.awaitTermination(10L, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		grid = new_board.clone();
		new_board = null;
		
		// TRY RUNNABLES WITH EXEC SERVICE AGAIN
	}
	
	
	private class Updater extends RecursiveAction {
		private int start, end, size;
		private byte[][] old_grid, new_grid;
		
		public Updater(int s, int e, int size, byte[][] o, byte[][] n){
			this.start = s;
			this.end = e;
			this.size = size;
			this.old_grid = o;
			this.new_grid = n;
		}
		
		private int wrap(int pos){
			if(pos < 0)
				return size - 1;
			if(pos > size - 1)
				return 0;
			else
				return pos;
		}
		
		@Override
		public void compute(){
			if((end - start) <= rows_per_block){
				//start = start row, end = end row
				for(int y = start; y < end; y++){
					int y1 = wrap(y-1);
					int y2 = wrap(y+1);
					for(int x = 0; x < size; x++){
						
						int count = 0;					
						
						int x1 = wrap(x-1);
						int x2 = wrap(x+1);
						
						count = old_grid[y1][x1] + old_grid[y1][x] + old_grid[y1][x2] +
									old_grid[y][x1] + old_grid[y][x2] + 
									old_grid[y2][x1] + old_grid[y2][x] + old_grid[y2][x2];
		
								
						if(count < 2 || count  > 3){ 
							new_grid[y][x] = 0;
						} else if(count == 3){
							new_grid[y][x] = 1;
						} else {
							new_grid[y][x] = old_grid[y][x];
						}
					}
				}
			} else {
				int split = (start+end) >>> 1;
				invokeAll(new Updater(start, split, size, grid, new_grid), 
						new Updater(split, end, size, grid, new_grid));
			}
		}
	}
}
