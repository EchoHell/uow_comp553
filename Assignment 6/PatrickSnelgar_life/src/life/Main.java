package life;

import java.io.InputStream;

/**
 * Example main program for running the sequential Game-of-Life.
 *
 * @author Mark.Utting
 *
 */
public class Main {

	final static int GENERATIONS = 20; //4080
	final static int BOARD_SIZE = 32768; //1024
	
	public static void main(String[] args) throws Exception{
		String pat = "/gosperGliderGun.patt";
		int reps = 5;
		ParallelLife life;
		System.out.println("Doing "+GENERATIONS+" generations of "+BOARD_SIZE+"x"+BOARD_SIZE);
		int maxThreads = Runtime.getRuntime().availableProcessors();
		for(int startThreads = 1; startThreads <= maxThreads; startThreads+=4){
			long total_time = 0;
			for(int rep = 0; rep < reps; rep++){
				InputStream s = Main.class.getResourceAsStream(pat);
				life = new ParallelLife(BOARD_SIZE, startThreads);
				life.parsePattern(s);
				final long start_time = System.currentTimeMillis();
				for(int rec = 0; rec < GENERATIONS; rec++) {
					life.tick();
				}
				final long end_time = System.currentTimeMillis();
				//life.printBoard(40, 40);
				//life.shutDown();
				total_time += (end_time - start_time);
			}
			System.out.println("Time taken for " + GENERATIONS + " generations with " +startThreads+" threads of " + BOARD_SIZE 
					+ "x" + BOARD_SIZE + " is: " + ((total_time / reps) / 1e3) + "secs.");
			if(startThreads == 1) startThreads--;
		}
		
		
		
	}
}