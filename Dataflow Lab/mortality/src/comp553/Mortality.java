package comp553;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Example of how to define concurrent data-flow programs in Java.
 *
 * This program calculates working years, death date, superannuation contributions,
 * and then estimates your super balance at retirement and how much you have to live
 * on during retirement.
 * 
 * Input is a sequence of 'key=value' pairs, in any order, followed by 'exit'.
 *
 * @author Mark Utting
 */
public class Mortality {
	// System.out.format strings for the output messages.
	protected static final String DIE_MSG = "Die at %d\n";
	protected static final String WORKING_YEARS_MSG = "Working years=%d\n";
	protected static final String RETIREMENT_YEARS_MSG = "Retirement years=%d\n";
	protected static final String SUPER_PAYOUT_MSG = "Super payout=%.1f (median salaries)\n";
	protected static final String LIFESTYLE_MSG = "You live on %.1f%% of median salary\n";

	ExecutorService pool = Executors.newCachedThreadPool();
	ConcurrentMap<String, String> blackboard = new ConcurrentHashMap<String, String>();

	/**
	 * Calculates your superannuation balance and your retirement lifestyle.
	 *
	 * @param args
	 * @throws IOException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
		Mortality prog = new Mortality();
		Future<Double> result = prog.createGraph();
//		InputStream ins = Mortality.class.getResourceAsStream("/input.txt");
//		System.out.println(ins);
//      //Some example fixed input, to make it convenient to rerun the program for testing
//		final String input = ""
//			+ "birthyear=1990\n"
//			+ "sex=male\n"
//			+ "exit\n";
//		ByteArrayInputStream bis = new ByteArrayInputStream(input.getBytes());
//		InputStreamReader input = new InputStreamReader(ins);
		InputStreamReader input = new InputStreamReader(System.in);
		prog.readInput(input);
		prog.displayResult(result);
	}
	
	/**
	 * Creates the dataflow graph.
	 *
	 * @return the output of the final node (a Future object).
	 */
	public Future<Double> createGraph() {
		// set up all the input readers
		Future<Integer> birthyear = pool.submit(new GetInt(blackboard, "birth year"));       // the year you were born
		Future<String> sex = pool.submit(new GetString(blackboard, "sex"));                  // male/female
		Future<Integer> startsuper = pool.submit(new GetInt(blackboard, "start super age")); // the age you start contributing to your super fund.
		Future<Integer> retirement = pool.submit(new GetInt(blackboard, "retirement age"));  // the age you decide to retire
		Future<String> strategy = pool.submit(new GetString(blackboard, "super strategy"));  // growth/balanced/conservative/cash
		Future<Integer> contribution = pool.submit(new GetInt(blackboard, "contribution%")); // including the employer contribution
		// create the dataflow nodes and graph
		Future<Integer> deathage = pool.submit(new DeathAge(birthyear, sex));
		Future<Integer> workingYears = pool.submit(new WorkingYears(startsuper, retirement));
		Future<Integer> retirementYears = pool.submit(new RetirementYears(retirement, deathage));
		Future<Double> superbalance = pool.submit(new SuperBalance(workingYears, contribution, strategy));
		Future<Double> lifestyle = pool.submit(new LifeStyle(retirementYears, superbalance));
		pool.shutdown(); // do not accept any new tasks
		return lifestyle;
	}

	/**
	 * Reads key=value pairs from the input stream.
	 * The (key,value) pairs are put into <code>blackboard</code>.
	 *
	 * @throws IOException
	 */
	public void readInput(InputStreamReader input) throws IOException {
		System.out.println("Now enter 'key=value' pairs:");
		BufferedReader reader = new BufferedReader(input);
		for (;;) {
			String line = reader.readLine();
			if (line == null || line.equals("exit")) {
				break;
			}
			String[] words = line.split("=");
			if (words.length != 2) {
				System.err.println("Warning: Input lines must have the format: key=value");
			} else {
				final String key = words[0].trim();
				final String value = words[1].trim();
				// System.out.println("DEBUG: adding " + key + " = " + value);
				if (blackboard.putIfAbsent(key, value) != null) {
					System.err.println("Error: key '" + key + "' defined more than once.");
					System.exit(2);
				}
			}
		}
	}
	
	/**
	 * Displays some final output messages, given the final node of the dataflow graph.
	 *
	 * @param lifestyle the Future object that is the output of the dataflow graph.
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public void displayResult(Future<Double> lifestyle) throws InterruptedException, ExecutionException {
		// Get the final output of the dataflow calculation and analyze it.
		final double spendLevel = lifestyle.get();
		// Median income in New Zealand is $41,900.
		// See http://www.superannuation.asn.au/resources/retirement-standard
		// Modest single lifestyle requires $22,858, which is about 0.5 of median income.
		// Comfortable lifestyle requires $41,186, which is about same as median income.
		if (spendLevel < 0.5) {
			System.out.format("Miserable poverty...");
		} else if (spendLevel < 1.0) {
			System.out.format("A modest lifestyle...");
		} else {
			System.out.format("Comfortable!...");
		}
	}
	
	/**
	 * A dataflow node that returns a given constant result.
	 * @param <T>
	 */
	public static class Constant<T> implements Callable<T> {
		private final T mResult;
		public Constant(T result) {
			mResult = result;
		}
		public T call() throws Exception {
			return mResult;
		}
	}

	/**
	 * A dataflow node that estimates an expected age at which you will die.
	 * Based on average life expectancy tables from Statistics NZ.
	 * http://www.stats.govt.nz/browse_for_stats/health/life_expectancy/NZLifeTables_HOTP10-12.aspx
	 *
	 * @author Mark Utting
	 */
	public static class DeathAge implements Callable<Integer> {
		private final Future<Integer> mBirthYear;
		private final Future<String> mSex;

		public DeathAge(Future<Integer> year, Future<String> sex) {
			mBirthYear = year;
			mSex = sex;
		}

		public Integer call() throws Exception {
			Integer birthyear = mBirthYear.get();
			String sex = mSex.get().toLowerCase();
			final int deathAge;
			if ("male".equals(sex)) {
				deathAge = 79 + (2011 - birthyear) / 20;
			} else {
				deathAge = 83 + (2011 - birthyear) / 30;
			}
			System.out.format(DIE_MSG, deathAge); // approximate.
			return deathAge;
		}
	}
	
	/**
	 * A dataflow node that calculates the amount of years a person will work.
	 * Calculation is retirement age - start super age.
	 * 
	 * @author Patrick
	 */
	public static class WorkingYears implements Callable<Integer> {
		private final Future<Integer> mRetirementAge;
		private final Future<Integer> mStartWorkAge;
		
		public WorkingYears(Future<Integer> sAge, Future<Integer> rAge){
			mStartWorkAge = sAge;
			mRetirementAge = rAge;
		}
		
		public Integer call() throws Exception {
			Integer retirement = mRetirementAge.get();
			Integer startsuper = mStartWorkAge.get();
			final int workingYears;
			workingYears = retirement - startsuper;
			System.out.format(WORKING_YEARS_MSG, workingYears);
			return workingYears;
		}
	}

	/**
	 * A dataflow node that calculates the time spent in retirement.
	 * Calculation uses expected death age from DeathAge node - the given retirement age. 
	 * 
	 * @author Patrick
	 */
	public static class RetirementYears implements Callable<Integer> {
		private final Future<Integer> mRetirementAge;
		private final Future<Integer> mDeathAge;
		
		public RetirementYears(Future<Integer> rAge, Future<Integer> dAge){
			mRetirementAge = rAge;
			mDeathAge = dAge;
		}
		
		public Integer call() throws Exception{
			Integer retirementage = mRetirementAge.get();
			Integer deathage = mDeathAge.get();
			final int retirementYears;
			retirementYears = deathage - retirementage;
			System.out.format(RETIREMENT_YEARS_MSG,retirementYears);
			return retirementYears;
		}
	}
	
	/**
	 * A dataflow node that calculates the retirement balanced.
	 * Calculation is based on balance * performance(strat) + contribution / 100
	 * where performance(strat) is:
	 * 	growth = 		0.045
	 * 	balanced = 		0.035
	 * 	conservative = 	0.25
	 * 	cash =	 		1.0
	 * 
	 * and contribution is the percentage contribution / 100
	 * 
	 * @author Patrick
	 */
	public static class SuperBalance implements Callable<Double> {
		private final Future<Integer> mWorkingYears;
		private final Future<String> mStrategy;
		private final Future<Integer> mContribution;
		
		public SuperBalance(Future<Integer> wYears, Future<Integer> contr, Future<String> strat){
			mWorkingYears = wYears;
			mStrategy = strat;
			mContribution = contr;
		}
		
		public Double call() throws Exception {
			Integer years = mWorkingYears.get();
			String superStrat = mStrategy.get().toLowerCase();
			Integer contribution = mContribution.get();
			
			double stratMulti;
			switch(superStrat){
				case "growth":
					stratMulti = 0.045;
					break;
				case "balanced":
					stratMulti = 0.035;
					break;
				case "conservative":
					stratMulti = 0.025;
					break;
				default:
					stratMulti = 1.0;
					break;
			}
			
			final double superbalance;
			double tempBalance = 0;
			for(int i = 0; i < years; i++){
				tempBalance += tempBalance * stratMulti + contribution / 100.0;
			}
			superbalance = tempBalance;
			System.out.format(SUPER_PAYOUT_MSG, superbalance);
			return superbalance;
		}
	}
	
	/**
	 * A dataflow node that is used to calculate a values to base the lifestyle choice on.
	 * Calculation is based on the super balance / number of years in retirement.
	 * 
	 * @author Patrick
	 */
	public static class LifeStyle implements Callable<Double> {
		private final Future<Integer> mRetirementYears;
		private final Future<Double> mSuperBalance;
		
		public LifeStyle(Future<Integer> rYears, Future<Double> sBal){
			mRetirementYears = rYears;
			mSuperBalance = sBal;
		}
		
		public Double call() throws Exception {
			Integer years = mRetirementYears.get();
			Double bal = mSuperBalance.get();
			
			final double life;
			life = bal / years;
			System.out.format(LIFESTYLE_MSG, (life*100));
			return life;
		}
	}
	
	/**
	 * A dataflow node that waits for a given key to appear in the blackboard.
	 * Then it returns the value part of the key=value pair, as an integer.
	 * Exceptions will be thrown if that value string is not an integer.
	 * 
	 * @author Mark.Utting
	 */
	public static class GetInt implements Callable<Integer> {
		private final GetString mString;
		public GetInt(ConcurrentMap<String,String> map, String key) {
			mString = new GetString(map, key);
		}
		public Integer call() throws Exception {
			final String str = mString.call();
			return Integer.parseInt(str);
		}
	}

	/**
	 * A dataflow node that waits for a given key to appear in the blackboard.
	 * Then it returns the value part of the key=value pair, as a String.
	 *
	 * @author Mark.Utting
	 */
	public static class GetString implements Callable<String> {
		private final String mKey;
		private ConcurrentMap<String,String> mMap;
		public GetString(ConcurrentMap<String,String> map, String key) {
			mKey = key;
			mMap = map;
		}
		public String call() throws Exception {
			for (;;) {
				// System.out.println("   looking for key " + mKey + "...");
				if (mMap.containsKey(mKey)) {
					return mMap.get(mKey);
				}
				Thread.sleep(200); // 0.2 seconds
			}
		}
	}
}
