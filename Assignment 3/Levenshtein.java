package spel_coreckion;

/**
 * Calculate the minimum edit distance between two strings.
 *
 * Algorithm adapted from <a href="http://en.wikipedia.org/wiki/Levenshtein_distance">
 * Wikipedia Levenshtein Distance</a> entry.
 *
 * This is a simple example of dynamic programming to compute the edit distance.
 * 
 * @author Mark Utting
 */
public class Levenshtein {
	
	final static int REPLACE_COST = 1;
	final static int INSERT_COST = 1;
	final static int DELETE_COST = 1;

	private int[][] distance = new int[30][50];

	/**
	 * Set up the matrices with the maximum string sizes.
	 */
	public Levenshtein() {
	}

	public int analyze(String sx, String sy) {
		if (sx.length() >= distance.length || sy.length() >= distance[0].length) {
			System.err.println("EXPANDED EDIT DISTANCE TO " + (sx.length() + 1) + ", " + (sy.length() + 1));
			distance = new int[sx.length() + 1][sy.length() + 1];
		}
	   // for all i and j, distance[i][j] will hold the Levenshtein distance between
	   // the first i characters of sx and the first j characters of sy;
	   // note that d has (m+1)*(n+1) values

	   for (int x = 0; x < distance.length; x++) {
		   distance[x][0] = x; // the distance of any first string to an empty second string
	   }
	   for (int y = 0; y < distance[0].length; y++) {
		   distance[0][y] = y; // the distance of any second string to an empty first string
	   }
	   for (int x = 1; x <= sx.length(); x++) {
	     for (int y = 1; y <= sy.length(); y++) {
	       if (sx.charAt(x - 1) == sy.charAt(y - 1)) {  
	    	   distance[x][y] = distance[x - 1][y - 1]; // no operation required
	       } else {
	    	   final int deleteCost = distance[x - 1][y] + DELETE_COST;
	    	   final int insertCost = distance[x][y - 1] + INSERT_COST;
	    	   final int replaceCost = distance[x - 1][y - 1] + REPLACE_COST;
	    	   distance[x][y] = Math.min(deleteCost, Math.min(insertCost, replaceCost));
	    	}
	     }
	   }
	   return distance[sx.length()][sy.length()];
	 }

	public static void main(String[] arg) {
		final Levenshtein ed = new Levenshtein();
		System.out.println("distance of pretty / pretty = " + ed.analyze("pretty", "pretty"));
		System.out.println("distance of pretty / pratty = " + ed.analyze("pretty", "pratty"));
		System.out.println("distance of pretty / praty = " + ed.analyze("pretty", "praty"));
		System.out.println("distance of pratt / pretty = " + ed.analyze("pretty", "praty"));
		System.out.println("distance of labored / laboured = " + ed.analyze("labored", "laboured"));
	}
}
