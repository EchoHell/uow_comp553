package pi;

import org.junit.Test;

import junit.framework.TestCase;

public class dummThrowDartsTest extends TestCase {

	@Test
	public void testFirstThread() throws Exception{
		Pi _testPI = new Pi();
		int[] results = new int[6];
		_testPI.dummyThrowDarts(new int[] {0,1,2,3,4,5}, 1000, results, 0);
		assertTrue(results[0] > 750);
		assertEquals(results[1] , 0);
		assertEquals(results[2] , 0);
		assertEquals(results[3] , 0);
		assertEquals(results[4] , 0);
		assertEquals(results[5] , 0);
	}
	
	@Test
	public void testLastThread() throws Exception {
		Pi _testPI = new Pi();
		int[] results = new int[6];
		_testPI.dummyThrowDarts(new int[] {0,1,2,3,4,5}, 1000, results, 5);
		assertTrue(results[5] > 750);
		assertEquals(results[1] , 0);
		assertEquals(results[2] , 0);
		assertEquals(results[3] , 0);
		assertEquals(results[4] , 0);
		assertEquals(results[0] , 0);
	}
}
